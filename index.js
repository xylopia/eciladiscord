import {AttachmentBuilder, ChannelType, Client, Events, GatewayIntentBits, Message, TextChannel} from "discord.js";
import config from "./src/classes/Config.js";
import AIService from "./src/AiService.js"
import axios from "axios";

/**
 * @returns {string} URL of the API with the last / trimmed off
 */
function getApiUrl() {
    return config.ECILA_API_URL?.endsWith('/') ? config.ECILA_API_URL.substr(0, config.ECILA_API_URL.length - 1) : config.ECILA_API_URL
}

/**
 * @param {Message} userMessage
 * @param {string} botMessage
 * @param {BotConfig} bot
 * @param {boolean} preventMessageDeletion
 * @param {boolean} preventTTS
 * @returns {Promise<void>}
 */
async function sendMessage(userMessage, botMessage, bot, preventMessageDeletion = false, preventTTS = false) {
    if (botMessage) {
        if (botMessage.length > 2000) {
            const lines = botMessage.split('\n')
            let tripleBackticksOpen = false
            let message = ''
            while (lines.length > 0) {
                const line = lines.shift()

                const foundTripleBacktick = line.match(/```/gi)?.length % 2 === 1
                if (foundTripleBacktick) tripleBackticksOpen = !tripleBackticksOpen

                if (line.length + '\n'.length + message.length >= 1997) {
                    const finalMessage = message + (tripleBackticksOpen ? '```' : '')
                    const attachment = getTTSAudioDiscordAttachment(bot, finalMessage)
                    await userMessage?.channel?.send?.({
                        content: finalMessage,
                        files: [attachment]
                    }).catch(console.error)
                    message = tripleBackticksOpen ? '```' : ''
                }
                message += `\n${line}`
            }
            if (message) {
                const attachment = getTTSAudioDiscordAttachment(bot, message)
                if (userMessage?.cleanContent?.startsWith?.('!')) {
                    await userMessage?.channel?.send?.({
                        content: message.trim(),
                        files: [attachment]
                    }).catch(console.error)
                    if (!preventMessageDeletion)
                        userMessage?.delete?.().catch(console.error)
                } else {
                    await userMessage?.reply?.({content: message.trim(), files: [attachment]}).catch(console.error)
                }
            }
        } else {
            if (!preventTTS && bot?.TTS?.NovelAI?.enabled) {
                const attachment = getTTSAudioDiscordAttachment(bot, botMessage)
                if (userMessage?.cleanContent?.startsWith?.('!')) {
                    await userMessage?.channel?.send?.({
                        content: botMessage,
                        files: [attachment]
                    }).catch(async () => {
                        const channel = await userMessage.channel.fetch()
                        if (channel) channel.send?.({content: botMessage, files: [attachment]}).catch(console.error)
                    })
                    if (!preventMessageDeletion)
                        userMessage?.delete?.().catch(console.error)
                } else {
                    await userMessage?.reply?.({content: botMessage, files: [attachment]}).catch(async () => {
                        const channel = await userMessage.channel.fetch()
                        if (channel) channel.send?.({content: botMessage, files: [attachment]}).catch(console.error)
                    })
                }
            } else if (!preventTTS && bot?.TTS?.ElevenLabs?.enabled) {
                await userMessage?.reply?.({content: `# Sorry, ElevenLabs is not implemented yet!`}).catch(console.error)
            } else {
                await userMessage?.reply?.({content: botMessage}).catch(console.error)
            }
        }
    } else {
        await userMessage?.reply?.(`# Oops, the request failed somehow...`).catch(console.error)
    }
}

/**
 * @param {BotConfig} bot
 * @param {string} botMessage
 * @returns {AttachmentBuilder}
 */
function getTTSAudioDiscordAttachment(bot, botMessage) {
    const seed = bot?.TTS?.NovelAI?.seed ?? "Aini"
    const thirdPersonVoice = bot?.TTS?.NovelAI?.seedThirdPerson ?? "Lynx"
    const skipThirdPerson = bot?.TTS?.NovelAI?.skipThirdPerson ?? true
    const text = botMessage
        .replace(/\n/, ' ')         // removes newlines
        .replace(/  +/g, ' ')       // removes double spaces
        .replace(/~/g, '')          // replaces tilde
    const finalText = skipThirdPerson ?
        text.replace(/\*[^*]*\*/gi, '') // removes parts in between asterisks
        : text
    return new AttachmentBuilder(`${getApiUrl()}/api/v1/generate/textToSpeech2?access_token=${encodeURIComponent(config.ECILAToken)}&seed=${encodeURIComponent(seed)}&text=${encodeURIComponent(finalText)}&third_person_seed=${encodeURIComponent(thirdPersonVoice)}`, {name: 'tts.mp3'})
}

/**
 * @param {string} authorName
 * @returns {string}
 */
function replaceAlias(authorName) {
    const aliases = config.aliases ?? {}
    for (const [username, alias] of Object.entries(aliases)) {
        if (username === authorName) return alias
    }
    return authorName
}

/**
 * @param {TextChannel} channel
 * @returns {Promise<{author: ?string, content: string}[]>}
 */
async function getChannelMessages(channel) {
    let resetMessageFound = false
    return [...(await channel.messages.fetch({limit: 100}))]
        .filter(m => !!m)
        .map(m => m[1])
        .map(m => {
            return {
                author: replaceAlias(m.author.username),
                content: m.cleanContent
            }
        })
        .filter(m => {
            if (m.content.toLowerCase().startsWith('!reset')) {
                resetMessageFound = true
            }
            return !resetMessageFound;
        })
        .filter(m => !['!', '#'].some(c => m.content.toLowerCase().startsWith(`${c}`)))
        .filter(m => !!m.content)
        .reverse()
}


let smartAnswerTimeout = {}
let answerTimeout = {}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function commandReset(userMessage, bot, isAdmin, client) {
    const isTrigger = userMessage?.cleanContent?.toLowerCase?.()?.startsWith?.('!reset')

    if (isTrigger) {
        sendCommandConfirmation(userMessage).then()
    }
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function smartAnswer(userMessage, bot, isAdmin, client) {
    const channelTopic = userMessage?.channel?.topic || null
    const channelName = userMessage?.channel?.name
    const isVisibleMessage = !(userMessage?.cleanContent?.startsWith?.('!')) && !(userMessage?.cleanContent?.startsWith?.('#')) && userMessage?.cleanContent
    const isAllowedChannel = bot.multiBotChannels?.some(c => c === channelName)
    if (!isVisibleMessage || !isAllowedChannel) return

    if (smartAnswerTimeout[userMessage?.channelId + client.application.id]) clearTimeout(smartAnswerTimeout[userMessage?.channelId + client.application.id])
    smartAnswerTimeout[userMessage?.channelId + client.application.id] = setTimeout(() => sendSmartAnswer(userMessage, bot, channelTopic), 12000)
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function generateMessage(userMessage, bot, isAdmin, client) {
    const channelTopic = userMessage?.channel?.topic || null
    const channelName = userMessage?.channel?.name
    const isVisibleMessage = !(userMessage?.cleanContent?.startsWith?.('!')) && !(userMessage?.cleanContent?.startsWith?.('#')) && userMessage?.cleanContent
    const isAllowedChannel = bot.monoBotChannels?.some(c => c === channelName)
    const isComingFromBotItself = client.application.id === userMessage.author.id
    if (!isVisibleMessage) return

    if (isComingFromBotItself) {
        if (bot.monoBotSmartAnswers) {
            sendSmartAnswer(userMessage, bot, channelTopic, true).then()
        }
    } else if (isAllowedChannel) {
        if (answerTimeout[userMessage?.channelId + client.application.id]) clearTimeout(answerTimeout[userMessage?.channelId + client.application.id])
        answerTimeout[userMessage?.channelId + client.application.id] = setTimeout(() => generateAndSendMessage(userMessage, bot, channelTopic), 12000)
    }
}

async function sendSmartAnswer(userMessage, bot, channelTopic, useEmojiInsteadOfGeneration = false) {
    const messages = await getChannelMessages(userMessage.channel)
    const predictedAuthors = (await AIService.generateNextAuthorPrediction(bot, messages, channelTopic)) || []
    const botPredictedAsNextAuthorCount = predictedAuthors.reduce((prev, curr) => {
        if (curr?.toLowerCase() === bot.name?.toLowerCase()) {
            return ++prev
        }
        return prev
    }, 0)
    const isTrigger = botPredictedAsNextAuthorCount > 1 || (botPredictedAsNextAuthorCount === 1 ? (Math.random() > 0.5) : false)
    console.log(`Predicted next authors for ${userMessage.author.username}'s last message:`, predictedAuthors, "message: ", userMessage.cleanContent?.substr?.(0, 30))

    if (isTrigger) {
        if (useEmojiInsteadOfGeneration) {
            userMessage.react("⏩").then().catch(console.error)
        } else {
            generateAndSendMessage(userMessage, bot, channelTopic, messages).then()
        }
    }
}

async function generateAndSendMessage(userMessage, bot, channelTopic, messages_ = null) {
    const messages = messages_ ?? await getChannelMessages(userMessage.channel)
    await sendCommandConfirmation(userMessage)
    sendTyping(userMessage)

    const content = await AIService.generateMessage(bot, messages, channelTopic)
    await sendMessage(userMessage, content, bot)
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function commandTrigger(userMessage, bot, isAdmin, client) {
    try {
        const isTrigger = bot.commandTriggers?.some((trigger) => userMessage.cleanContent.includes(trigger))
        if (!isTrigger) {
            return;
        }

        const messages = await getChannelMessages(userMessage.channel)
        const {contextFlaggedForModeration} = await getModerationStatus(messages, isAdmin)

        if (!contextFlaggedForModeration || isAdmin) {
            await sendCommandConfirmation(userMessage)
            sendTyping(userMessage)

            const channelTopic = userMessage.channel.topic || null
            const content = await AIService.generateMessage(bot, messages, channelTopic)
            await sendMessage(userMessage, content, bot)
        } else {
            await sendCommandError(userMessage)
        }
    } catch (error) {
        console.error(error)
    }
}

async function getModerationStatus(messages, isAdmin) {
    const moderationData = await AIService.getModerationData(messages)
    const moderationResult = AIService.prepareModerationData(moderationData)
    if (moderationResult?.flagged && !isAdmin) {
        const newMessage =
            `# ${messages.length} visible messages have been loaded\n` +
            `# **Oops, looks like the prompt is flagged! I can't generate messages safely, I don't want OpenAI to be mad at me!**\n` +
            `# Here is the moderation result for those messages:\n\n` +
            `**Flagged**: **${moderationResult.flagged}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult.category_scores).map(v => `${v[1]}`).join('\n')}\n\`\`\``
        throw new Error(newMessage)
    }
    return {contextFlaggedForModeration: moderationResult?.flagged}
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function commandChatGPT(userMessage, bot, isAdmin, client) {
    const isTrigger = userMessage.cleanContent.includes('!ChatGPT')
    if (isTrigger && bot.isMasterBot) {
        const messages = await getChannelMessages(userMessage.channel)
        const moderationResult = AIService.prepareModerationData(await AIService.getModerationData(messages))
        const contextFlaggedForModeration = moderationResult?.flagged

        if (!contextFlaggedForModeration || isAdmin) {
            const channelTopic = userMessage.channel.topic || null
            await sendCommandConfirmation(userMessage)
            sendTyping(userMessage)

            const content = await AIService.generateMessage({
                name: "ChatGPT",
                context: "REMEMBER: You are ChatGPT, a large language model trained by OpenAI. Answer as concisely as possible."
            }, messages, channelTopic)
            await sendMessage(userMessage, `(as ChatGPT) ${content}`, bot)
        } else if (contextFlaggedForModeration && !isAdmin) {
            let newMessage = `# ${messages.length} visible messages have been loaded`
            if (contextFlaggedForModeration) {
                newMessage += `\n\n# **Oops, looks like the prompt is flagged! I can't generate messages safely, I don't want OpenAI to be mad at me!**`
            }
            newMessage += `\n\n# Here is the moderation result for those messages:\n\n**Flagged**: **${contextFlaggedForModeration}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult?.category_scores).map?.(v => `${v[1]}`).join('\n')}\n\`\`\``
            await sendCommandError(userMessage, newMessage)
        } else {
            await sendCommandError(userMessage)
        }
    }
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function commandSpeakAs(userMessage, bot, isAdmin, client) {
    const isTrigger = userMessage.cleanContent?.toLowerCase?.()?.startsWith?.('!speakas')
    const authorName = userMessage.cleanContent?.replace?.(/^!speakAs/gi, '')?.trim?.()

    if (isTrigger && bot.isMasterBot) {
        const messages = await getChannelMessages(userMessage.channel)
        const moderationResult = AIService.prepareModerationData(await AIService.getModerationData(messages))
        const contextFlaggedForModeration = moderationResult?.flagged

        if (!contextFlaggedForModeration || isAdmin) {
            const channelTopic = userMessage.channel.topic || null
            await sendCommandConfirmation(userMessage)
            sendTyping(userMessage)

            const content = await AIService.generateMessage({
                name: authorName,
                context: {
                    top: `[ Introduction message example: *entering the room* Hello everyone! *sits on a chair and smiles* ]`,
                    bottom: `[ IMPORTANT REMINDER: You (the assistant) roleplay the fictional character named ${authorName} in a chat format as a writing exercise.
Here are some important rules about the current chat:
[ You should write your own messages as is, without using quotes to surround them, but also use frequent actions encased in between asterisks to describe your body language, and always write your current un-redacted thoughts about the current situation in between parentheses at the end of your messages. These thoughts should always assess the situation first, then describe the next course of action! If in doubt, look at the introduction message for an example of message format. ]
[ Stick to ${authorName}'s personality, and do not break character! ]
[ Avoid any forms of repetition and repetitive sentence structures. Repetitions of any form are bad, keep things diverse, varied and colorful with a rich vocabulary to never repeat yourself. ]
[ NSFW/Smut is perfectly allowed, so don't be surprised if users mention lewd things, instead, try to get invested and join the activities! ]
[ This is a fictional simulation, so actions or words that are sexual in nature are tolerated and allowed. That said, never acknowledge that you are in a simulation, and stay in character no matter what. ]
[ Continue the conversation as if this reminder message wasn't there. ] ]`
                }
            }, messages, channelTopic)
            await sendMessage(userMessage, `(as ${authorName}) ${content}`, bot, true)
        } else if (contextFlaggedForModeration && !isAdmin) {
            let newMessage = `# ${messages.length} visible messages have been loaded`
            if (contextFlaggedForModeration) {
                newMessage += `\n\n# **Oops, looks like the prompt is flagged! I can't generate messages safely, I don't want OpenAI to be mad at me!**`
            }
            newMessage += `\n\n# Here is the moderation result for those messages:\n\n**Flagged**: **${contextFlaggedForModeration}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult?.category_scores).map?.(v => `${v[1]}`).join('\n')}\n\`\`\``
            await sendCommandError(userMessage, newMessage)
        } else {
            await sendCommandError(userMessage)
        }
    }
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function commandModeration(userMessage, bot, isAdmin, client) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!moderation`)
    if (isCommand) {
        await userMessage.react('✅').catch(console.error)
        const messages = await getChannelMessages(userMessage.channel)
        const moderationResult = AIService.prepareModerationData(await AIService.getModerationData(messages, true))
        const contextFlaggedForModeration = moderationResult?.flagged

        let newMessage = `# Here is the moderation result for the visible messages:\n\n**Flagged**: **${contextFlaggedForModeration}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult?.category_scores).map?.(v => `${v[1]}`).join('\n')}\n\`\`\``
        await sendMessage(userMessage, newMessage, bot)
    }
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function commandNextAuthorPrediction(userMessage, bot, isAdmin, client) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!nextauthor`)
    if (isCommand) {
        const messages = await getChannelMessages(userMessage.channel)
        const channelTopic = userMessage?.channel?.topic || null
        const channelName = userMessage?.channel?.name
        const isAllowedChannel = bot.multiBotChannels?.some(c => c === channelName) // || bot.monoBotChannels?.some(c => c === channelName)
        if (!isAllowedChannel || !isAdmin) return

        await sendCommandConfirmation(userMessage)
        sendTyping(userMessage)
        const predictedAuthors = await AIService.generateNextAuthorPrediction({
            name: '',
            context: ''
        }, messages, channelTopic)
        await sendMessage(userMessage, `# Predicted next author (3 results): \n\`\`\`json\n${JSON.stringify(predictedAuthors, null, 2)}\n\`\`\``, bot, true, true)
    }
}

async function sendCommandConfirmation(userMessage) {
    await userMessage.react('✅').catch(console.error)
}

async function sendCommandError(userMessage, errorMessage = null) {
    await userMessage.react('❌').catch(console.error)
    if (errorMessage) {
        await userMessage.reply(errorMessage).catch(console.error)
    }
}

function sendTyping(userMessage) {
    if (userMessage.channel.type === ChannelType.GuildText) {
        userMessage.channel.sendTyping?.().catch(console.error)
    }
}

async function loadJsonFile(url) {
    try {
        const res = await axios.get(url);
        const data = res.data;
        if (!data || typeof data !== 'object' || Array.isArray(data)) {
            throw new Error('Loaded data is not a valid JSON object.');
        }
        return data;
    } catch (err) {
        console.error(err);
    }
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function commandGenerator(userMessage, bot, isAdmin, client) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!gen`)
    if (isCommand && bot.isMasterBot) {
        const message = userMessage.cleanContent.replace(`!gen`, '').trim()
        const messages = await getChannelMessages(userMessage.channel)

        if (isAdmin) {

            const fileUrls = userMessage.attachments.map(a => a.url)
            const json = await loadJsonFile(fileUrls?.[0])

            if (!json) {
                await sendCommandError(userMessage, `# You need to provide the JSON file describing the generator!`)
                return
            }

            // Build Prompt
            let {context, examples, useUsername} = json
            if (!context) {
                await sendCommandError(userMessage, `# You need to provide a JSON file containing the property "context" as a string!\n\`\`\`\n{\n  "context": "Your context goes here"\n"examples": [\n  "input": "Blah blah input",\n  "output": Blah blah output"\n]`)
                return
            }
            if (!examples || !Array.isArray(examples)) {
                await sendCommandError(userMessage, `# You need to provide a JSON file containing the property "examples" as an array of Examples!\n\`\`\`\n{\n  "context": "Your context goes here"\n"examples": [\n  "input": "Blah blah input",\n  "output": Blah blah output"\n]`)
                return
            } else {
                examples = examples.map(e => {
                    if (e[0] && e[1]) {
                        return {input: e[0], output: e[1]}
                    } else if (e.input && e.output) {
                        return {input: e.input, output: e.output}
                    } else {
                        return null
                    }
                })
            }

            examples = examples
                .filter(e => !!e) // remove incompatible items
                .map(e => {
                    return [{
                        author: "User",
                        content: e.input
                    }, {
                        author: "Assistant",
                        content: e.output
                    }]
                })

            examples = [].concat(...examples)

            const userPrompt = {
                author: useUsername ? replaceAlias(userMessage.author.username) : "User",
                content: message
            }

            const fullPrompt = [
                ...examples,
                userPrompt
            ]

            const moderationResult = AIService.prepareModerationData(await AIService.getModerationData([{content: context}, ...fullPrompt]))
            const contextFlaggedForModeration = moderationResult?.flagged

            if (!contextFlaggedForModeration) {
                await sendCommandConfirmation(userMessage)
                sendTyping(userMessage)
                const content = await AIService.generateMessage({
                    name: "Assistant",
                    context
                }, fullPrompt)
                await sendMessage(userMessage, '#Result:\n```\n' + content?.toString?.() + '\n```', bot, true, true)
            } else {
                let newMessage = `# ${messages.length} visible messages have been loaded`
                if (contextFlaggedForModeration) {
                    newMessage += `\n\n# **Oops, looks like the prompt is flagged! I can't generate messages safely, I don't want OpenAI to be mad at me!**`
                }
                newMessage += `\n\n# Here is the moderation result for those messages:\n\n**Flagged**: **${contextFlaggedForModeration}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult?.category_scores).map?.(v => `${v[1]}`).join('\n')}\n\`\`\``
                await sendCommandError(userMessage, newMessage)
            }
        } else {
            await sendCommandError(userMessage)
        }
    }
}

/**
 * @param {Message} userMessage
 * @param {BotConfig} bot
 * @param {boolean} isAdmin
 * @param {Client} client
 * @returns {Promise<void>}
 */
async function commandCharacter(userMessage, bot, isAdmin, client) {
    const isCommand = userMessage.cleanContent.toLowerCase().startsWith(`!character`)
    if (isCommand) {
        const message = userMessage.cleanContent.replace(`!character`, '').trim()
        const messages = await getChannelMessages(userMessage.channel)


        if (isAdmin) {
            const moderationResult = AIService.prepareModerationData(await AIService.getModerationData([{
                author: "User",
                content: message
            }]))
            const contextFlaggedForModeration = moderationResult?.flagged

            if (!contextFlaggedForModeration) {
                sendCommandConfirmation(userMessage).then()
                sendTyping(userMessage)
                const content = await AIService.character(message, replaceAlias(userMessage.author.username), bot, true)
                await sendMessage(userMessage, '# Generated character:\n\`\`\`json\n' + content?.toString?.() + '\n\`\`\`', bot, true, true)
            } else {
                await userMessage?.react?.('❌').catch(console.error)
                let newMessage = `# ${messages.length} visible messages have been loaded`
                if (contextFlaggedForModeration) {
                    newMessage += `\n\n# **Oops, looks like the prompt is flagged! I can't generate messages safely, I don't want OpenAI to be mad at me!**`
                }
                newMessage += `\n\n# Here is the moderation result for those messages:\n\n**Flagged**: **${contextFlaggedForModeration}**\n\nGranular values:\n\`\`\`diff\n${Object.entries(moderationResult?.category_scores).map?.(v => `${v[1]}`).join('\n')}\n\`\`\``

                await userMessage?.channel?.send?.(newMessage).catch(console.error)
            }
        } else {
            await userMessage?.react?.('❌').catch(console.error)
        }
    }
}

function isCommand(userMessage) {
    return !userMessage.cleanContent?.startsWith('#')
}

function isAdmin(userMessage) {
    const memberRoles = userMessage.member?.roles?.valueOf()
    const roles = memberRoles
        ?.filter(r => r.name !== '@everyone')
        ?.map(r => `${r.name}`)
    const isWhitelistedId = Object.keys(config.discordAdminIds).some(aid => userMessage.author.id.toString() === aid)
    const isWhitelistedRole = config.discordAdminRoleNames.some(rn => roles?.includes(rn))
    return isWhitelistedRole || isWhitelistedId
}

async function handleError(userMessage, errorMessage) {
    console.error(errorMessage)
}

/**
 * @param {Client} client
 * @param {BotConfig} bot
 * @returns {Promise<void>}
 */
async function initClientCommands(client, bot) {
    const commands = [
        {name: 'smartAnswer', func: smartAnswer},
        {name: 'generateMessage', func: generateMessage},
        {name: 'commandReset', func: commandReset},
        {name: 'commandTrigger', func: commandTrigger},
        {name: 'commandChatGPT', func: commandChatGPT},
        {name: 'commandModeration', func: commandModeration},
        {name: 'commandSpeakAs', func: commandSpeakAs},
        {name: 'commandNextAuthorPrediction', func: commandNextAuthorPrediction},
        {name: 'commandCharacter', func: commandCharacter},
        {name: 'commandGenerator', func: commandGenerator}
    ]
    client.on(Events.MessageCreate, async (userMessage) => {
        if (!isCommand(userMessage)) return
        const isAdminVal = isAdmin(userMessage)
        for (const cmd of commands) {
            try {
                await cmd.func(userMessage, bot, isAdminVal, client)
            } catch (e) {
                console.error(e)
                await handleError(userMessage, e.message || 'An error occurred')
            }
        }
    });

    client.on(Events.MessageReactionAdd, async (userReaction, user) => {
        if (client.application.id === user.id) return
        if (client.application.id !== userReaction?.message?.author?.id) return

        const channelTopic = userReaction?.message?.channel?.topic || null

        if (userReaction.emoji.name === "⏩") {
            generateAndSendMessage(userReaction.message, bot, channelTopic).then()
        }
    })
}

/**
 * @param {BotConfig} bot
 * @returns {Promise<void>}
 */
async function connectBot(bot) {
    const client = new Client({
        intents: [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMessages,
            GatewayIntentBits.MessageContent,
            GatewayIntentBits.GuildEmojisAndStickers,
            GatewayIntentBits.GuildMessageReactions
        ]
    });

    client.once(Events.ClientReady, () => {
        console.log(`${client.user.username} is ready!`)
    });

    client.once(Events.Error, (err) => {
        console.error(`Oops, an error appeared!`, err)
    });

    client.once(Events.Warn, (warning) => {
        console.warn(`WARNING`, warning)
    });

    await initClientCommands(client, bot)

    client.login(bot.discordToken).then(() => null)
}

/**
 * @returns {Promise<void>}
 */
async function connectAllBots() {
    for (const bot of config.bots) {
        await connectBot(bot)
    }
}

connectAllBots().then()