export default class BotConfig {
    /**
     * @param {string} discordToken
     * @param {string} name
     * @param {string} context
     * @param {?string} openAIContext
     * @param {?string} novelAIContext
     * @param {?{NovelAI: ?{enabled: boolean, seed: string, seedThirdPerson: ?string, skipThirdPerson: ?boolean}, ElevenLabs: ?{enabled: boolean}}} TTS
     * @param {?{NovelAI: ?*, OpenAI: ?*}} textGenParameters
     * @param {Array<string>} commandTriggers
     * @param {boolean} isMasterBot
     * @param {?string[]} monoBotChannels
     * @param {?string[]} multiBotChannels
     * @param {?string} mainBackend
     * @param {?boolean} monoBotSmartAnswers
     */
    constructor(discordToken, name, context, openAIContext, novelAIContext, TTS, textGenParameters, commandTriggers, isMasterBot, monoBotChannels, multiBotChannels, mainBackend, monoBotSmartAnswers) {
        this.discordToken = discordToken
        this.name = name
        this.context = context
        this.openAIContext = openAIContext
        this.novelAIContext = novelAIContext
        this.TTS = TTS
        this.textGenParameters = textGenParameters
        this.commandTriggers = commandTriggers
        this.isMasterBot = isMasterBot
        this.monoBotChannels = monoBotChannels
        this.monoBotSmartAnswers = monoBotSmartAnswers
        this.multiBotChannels = multiBotChannels
        this.mainBackend = mainBackend
    }
}