import {readFileSync} from "fs";

class Config {
    /**
     *
     * @param {string} ECILA_API_URL
     * @param {string} ECILAToken
     * @param {string} openAIToken
     * @param {string[]} discordAdminRoleNames
     * @param {string[]} discordAdminIds
     * @param {?Object} aliases
     * @param {BotConfig[]} bots
     * @param {?string} mainBackend
     */
    constructor(ECILA_API_URL, ECILAToken, openAIToken, discordAdminRoleNames, discordAdminIds, aliases, bots, mainBackend) {
        this.ECILA_API_URL = ECILA_API_URL
        this.ECILAToken = ECILAToken
        this.openAIToken = openAIToken
        this.discordAdminRoleNames = discordAdminRoleNames
        this.discordAdminIds = discordAdminIds
        this.aliases = aliases
        this.bots = bots
        this.mainBackend = mainBackend
    }
}

/**
 * @type {Config}
 */
const config = JSON.parse(readFileSync('./config.json').toString())

export default config