# EcilaDiscord

Noli's Discord.js bot is a unique application that empowers users to create their own AI bots for their Discord servers. Our bots use natural language processing and can understand and respond to user commands and messages in real-time! Whether it be as simple as greetings or something more complex like moderating conversations, our unique approach ensures that customizations can be made easily to suit your exact needs.

For more detailed infomation check here: https://gitlab.com/nolialsea/ecila-community

## Installation
- Install NodeJS v16 or more : https://nodejs.org/en/download/current
- Once installed goto the folder you wish to use the bot in and extract this reposity to it
- Open a command Prompt and run "npm install"
- Edit the configExample.json with your configs and credentials, and RENAME THE FILE to "config.json"
- open a command prompt and run "node index.js"

## Support
Discord: https://discord.gg/GuEXxPS3E8

## Contributing
This is Open Source, as such any contributions are greatly accepted.

## Authors and acknowledgment
Noli - Creator of the App